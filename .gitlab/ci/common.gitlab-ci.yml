.job-base: &job-base
  image: "registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker:${BUILDER_RUBY_DOCKER}"
  services:
  - docker:${DOCKER_VERSION}-dind
  needs: []
  retry: 1
  before_script:
    # Always compile assets for auto-deploy builds,
    # this is done for auto-deploy builds
    # so that we do not have to wait for the compile assets job
    # in the gitlab-ee pipeline.
    - |
      if [[ $CI_COMMIT_TAG =~ $AUTO_DEPLOY_TAG_REGEX ]]; then
        echo "Setting COMPILE_ASSETS env variable for auto-deploy"
        export COMPILE_ASSETS=true
      fi
    - mkdir -p artifacts/images artifacts/container_versions artifacts/final
    - source build-scripts/build.sh
    - if [[ "${CI_COMMIT_TAG}" == *-ubi8 || "${CI_COMMIT_REF_NAME}" == *-ubi8 ]]; then
    -   export UBI_PIPELINE="true"
    - elif [[ "${CI_COMMIT_TAG}" == *-fips || "${CI_COMMIT_REF_NAME}" == *-fips ]]; then
    -   export FIPS_PIPELINE="true"
    - fi
    - if [ "${FIPS_PIPELINE}" = "true" ]; then
    -   echo "UBI_IMAGE is '${UBI_IMAGE}'"
    -   export UBI_PIPELINE="true"
    -   export FIPS_MODE=1
    -   export GO_VERSION=${GO_FIPS_VERSION}
    -   export DOCKERFILE_EXT='.ubi8'
    -   export IMAGE_TAG_EXT='-fips'
    -   use_assets
    - elif [ "${UBI_PIPELINE}" = "true" ]; then
    -   echo "UBI_IMAGE is '${UBI_IMAGE}'"
    -   export DOCKERFILE_EXT='.ubi8'
    -   export IMAGE_TAG_EXT='-ubi8'
    -   use_assets
    - fi
    - export {CONTAINER_VERSION,BASE_VERSION}=$(get_version gitlab-base)
    - export TARGET_VERSION=$(get_target_version)
    - docker login -u "$CI_DEPENDENCY_PROXY_USER" -p "$CI_DEPENDENCY_PROXY_PASSWORD" "$CI_DEPENDENCY_PROXY_SERVER"
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - populate_stable_image_vars
  artifacts:
    paths:
      - artifacts/

.gitlab-base:
  extends: .job-base
  stage: phase-two
  script:
    # set CONTAINER_VERSION to include the distribution base image digests (only ever one present)
    - export CONTAINER_VERSION=($(echo -n "${TARGET_VERSION}${DEBIAN_DIGEST}${UBI_DIGEST}" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    -   logger_target_version=$(cat artifacts/container_versions/gitlab-logger_tag.txt)
    -   export GITLAB_LOGGER_VERSION=($(echo -n "$go_version$logger_target_version$GITLAB_LOGGER_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    -   gomplate_version=$(cat artifacts/container_versions/gitlab-gomplate_tag.txt)
    - fi
    - build_if_needed --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "GOMPLATE_TAG=${gomplate_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_LOGGER_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-logger:${GITLAB_LOGGER_VERSION}${IMAGE_TAG_EXT}"
                      $DEBIAN_BUILD_ARGS $UBI_BUILD_ARGS
    - push_tags
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-base_tag.txt

## phase one

.certificates:
  extends: .job-base
  stage: phase-two
  script:
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${gitlab_base_version}$GITLAB_VERSION" | sha1sum))
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - build_if_needed --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
                      $UBI_BUILD_ARGS
    - push_tags
    - push_tags ${GITLAB_VERSION}${IMAGE_TAG_EXT} ${CI_JOB_NAME/certificates/alpine-certificates}

.cfssl-self-sign:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION$CFSSL_VERSION" | sha1sum))
    - build_if_needed --build-arg "CFSSL_VERSION=$CFSSL_VERSION"
                      --build-arg "CFSSL_CHECKSUM_SHA256=$CFSSL_CHECKSUM_SHA256"
                      $ALPINE_BUILD_ARGS $UBI_BUILD_ARGS
    - push_tags
    - push_tags "${CFSSL_VERSION}${IMAGE_TAG_EXT}"

.kubectl:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${KUBECTL_VERSION}" | sha1sum))
    - build_if_needed --build-arg "KUBECTL_VERSION=${KUBECTL_VERSION}"
                      --build-arg "YQ_VERSION=${YQ_VERSION}"
                      $DEBIAN_BUILD_ARGS $UBI_BUILD_ARGS
    - push_tags
    - push_tags $KUBECTL_VERSION${IMAGE_TAG_EXT}

# debian only
.gitlab-gomplate:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${GOMPLATE_VERSION}" | sha1sum))
    - build_if_needed --build-arg "GOMPLATE_VERSION=$GOMPLATE_VERSION"
                      $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags $GOMPLATE_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-gomplate_tag.txt

# debian only
.gitlab-graphicsmagick:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${GM_VERSION}" | sha1sum))
    - export GM_VERSION_MINOR=$(expr match $GM_VERSION '\(^[0-9]\+.[0-9]\+\)')
    - build_if_needed --build-arg "GM_VERSION=$GM_VERSION"
                      --build-arg "GM_VERSION_MINOR=$GM_VERSION_MINOR"
                      --build-arg "GM_CHECKSUM_SHA256=$GM_CHECKSUM_SHA256"
                      $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags $GM_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-graphicsmagick_tag.txt

# debian only
.gitlab-exiftool:
  extends: .job-base
  stage: phase-three
  script:
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${gitlab_base_version}${EXIFTOOL_VERSION}" | sha1sum))
    - build_if_needed --build-arg "EXIFTOOL_VERSION=$EXIFTOOL_VERSION"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
                      $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags $EXIFTOOL_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-exiftool_tag.txt

# debian only
.gitlab-python:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION$PYTHON_VERSION" | sha1sum))
    - build_if_needed --build-arg "PYTHON_VERSION=${PYTHON_VERSION}"
                      $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags ${PYTHON_VERSION}${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-python_tag.txt

.gitlab-ruby:
  extends: .job-base
  stage: phase-three
  script:
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - rust_version=$(cat artifacts/container_versions/gitlab-rust_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - export CONTAINER_VERSION=($(echo -n "${TARGET_VERSION}${gitlab_base_version}${RUBY_VERSION}" | sha1sum))
    - build_if_needed --build-arg "RUBY_VERSION=$RUBY_VERSION"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
                      --build-arg "RUST_TAG=${rust_version}${IMAGE_TAG_EXT}"
                      $UBI_BUILD_ARGS $DEBIAN_BUILD_ARGS
    - push_tags
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-ruby_tag.txt

# debian only
.postgresql:
  extends: .job-base
  stage: phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION$PG_VERSION" | sha1sum))
    - build_if_needed --build-arg "PG_VERSION=$PG_VERSION"
                      $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags $PG_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/postgresql_tag.txt

## phase two

.gitlab-exporter:
  extends: .job-base
  stage: phase-four
  script:
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - ruby_container=($(echo -n "$ruby_version$GITLAB_EXPORTER_VERSION" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$ruby_container$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "GITLAB_EXPORTER_VERSION=$GITLAB_EXPORTER_VERSION"
                      --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-ruby"
                      --build-arg "TAG=$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "RUBY_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_EXPORTER_VERSION${IMAGE_TAG_EXT}

# debian only
.gitlab-go:
  extends: .job-base
  stage: phase-zero
  script:
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$TARGET_VERSION$GO_VERSION" | sha1sum))
    - build_if_needed --build-arg "GO_VERSION=$GO_VERSION"
                      $DEBIAN_BUILD_ARGS $UBI_BUILD_ARGS
    - push_tags
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-go_tag.txt

# debian only
.gitlab-rust:
  extends: .job-base
  stage: phase-zero
  script:
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$TARGET_VERSION$RUST_VERSION" | sha1sum))
    - build_if_needed --build-arg "RUST_VERSION=$RUST_VERSION"
                      $DEBIAN_BUILD_ARGS $UBI_BUILD_ARGS
    - push_tags
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-rust_tag.txt

.gitlab-mailroom:
  extends: .job-base
  stage: phase-four
  script:
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$ruby_version$TARGET_VERSION$MAILROOM_VERSION" | sha1sum))
    - build_if_needed --build-arg "MAILROOM_VERSION=$MAILROOM_VERSION"
                      --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-ruby"
                      --build-arg "TAG=$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "RUBY_IMAGE=$CI_REGISTRY_IMAGE/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $MAILROOM_VERSION${IMAGE_TAG_EXT}

## phase three

# debian only
.gitlab-logger:
  extends: .job-base
  stage: phase-one
  script:
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$go_version$TARGET_VERSION$GITLAB_LOGGER_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - build_if_needed --build-arg "GITLAB_LOGGER_VERSION=$GITLAB_LOGGER_VERSION"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_LOGGER_SHA256=${GITLAB_LOGGER_SHA256}"
    - push_tags
    - push_tags $GITLAB_LOGGER_VERSION${IMAGE_TAG_EXT}
    - echo -n "${TARGET_VERSION}" > artifacts/container_versions/gitlab-logger_tag.txt

## phase four

.gitaly:
  extends: .job-base
  stage: phase-four
  script:
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITALY_SERVER_VERSION$ruby_version$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:$go_version${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "GITALY_SERVER_VERSION=${GITALY_SERVER_VERSION}"
                      --build-arg "GITALY_GIT_REPO_URL=${GITALY_GIT_REPO_URL}"
                      --build-arg "TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "RUBY_TAG=$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "CACHE_BUSTER=$GITALY_SERVER_VERSION$(date -uI)"
                      --build-arg "RUBY_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITALY_SERVER_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitaly_tag.txt

.gitlab-container-registry:
  extends: .job-base
  stage: phase-four
  script:
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    -   go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - fi
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_CONTAINER_REGISTRY_VERSION$go_version$gitlab_base_version$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:$go_version${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "REGISTRY_VERSION=${GITLAB_CONTAINER_REGISTRY_VERSION}"
                      --build-arg "REGISTRY_NAMESPACE=gitlab-org"
    - push_tags
    - push_tags $GITLAB_CONTAINER_REGISTRY_VERSION${IMAGE_TAG_EXT}

.gitlab-zoekt-webserver:
  extends: .job-base
  stage: phase-four
  script:
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_ZOEKT_WEBSERVER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "ZOEKT_VERSION=${ZOEKT_VERSION}"
    - push_tags
    - push_tags $GITLAB_ZOEKT_WEBSERVER_VERSION${IMAGE_TAG_EXT}

.gitlab-zoekt-dynamic-indexserver:
  extends: .job-base
  stage: phase-four
  script:
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_ZOEKT_DYNAMIC_INDEXSERVER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "ZOEKT_VERSION=${ZOEKT_VERSION}"
    - push_tags
    - push_tags $GITLAB_ZOEKT_DYNAMIC_INDEXSERVER_VERSION${IMAGE_TAG_EXT}

# debian only
.gitlab-elasticsearch-indexer:
  extends:
    - .job-base
    - .only-ee
  stage: phase-four
  script:
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_ELASTICSEARCH_INDEXER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "GITLAB_ELASTICSEARCH_INDEXER_VERSION=${GITLAB_ELASTICSEARCH_INDEXER_VERSION}"
    - push_tags
    - push_tags "$GITLAB_ELASTICSEARCH_INDEXER_VERSION${IMAGE_TAG_EXT}"

.gitlab-metrics-exporter:
  extends: .job-base
  stage: phase-four
  script:
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_METRICS_EXPORTER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "VERSION=${GITLAB_METRICS_EXPORTER_VERSION}"
    - push_tags
    - push_tags "$GITLAB_METRICS_EXPORTER_VERSION${IMAGE_TAG_EXT}"
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-metrics-exporter_tag.txt

.gitlab-kas:
  extends: .job-base
  stage: phase-four
  script:
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export TARGET_VERSION=$(get_target_version)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_KAS_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:$go_version${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-go"
                      --build-arg "TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "GITLAB_KAS_VERSION=${GITLAB_KAS_VERSION}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      $UBI_BUILD_ARGS
    - push_tags
    - push_tags "$GITLAB_KAS_VERSION${IMAGE_TAG_EXT}"
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-kas_tag.txt

.gitlab-pages:
  extends: .job-base
  stage: phase-four
  script:
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - gomplate_version=$(cat artifacts/container_versions/gitlab-gomplate_tag.txt)
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - export CONTAINER_VERSION=($(echo -n "$gitlab_base_version$go_version$gomplate_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - build_if_needed --build-arg "GITLAB_PAGES_VERSION=$GITLAB_PAGES_VERSION"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GOMPLATE_TAG=${gomplate_version}${IMAGE_TAG_EXT}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "DEPENDENCY_PROXY=${DEPENDENCY_PROXY}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
                      --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_PAGES_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-pages_tag.txt

.gitlab-shell:
  extends: .job-base
  stage: phase-four
  script:
    - import_assets artifacts/ubi/{gitlab-logger,gitlab-gomplate}.tar.gz
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$go_version$TARGET_VERSION$GITLAB_SHELL_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:${go_version}${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_IMAGE=$CI_REGISTRY_IMAGE/gitlab-go:${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_SHELL_VERSION=${GITLAB_SHELL_VERSION}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "CACHE_BUSTER=$GITLAB_SHELL_VERSION$(date -uI)"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
                      $UBI_BUILD_ARGS $DEBIAN_BUILD_ARGS
    - push_tags
    - push_tags gitlab-$GITLAB_REF_SLUG${IMAGE_TAG_EXT}  # This can be removed during https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1553
    - push_tags $GITLAB_SHELL_VERSION${IMAGE_TAG_EXT}
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-shell_tag.txt

## phase five

.gitlab-rails-ee:
  extends:
    - .job-base
    - .only-ee
  stage: phase-five
  variables:
    RAILS_EE: "true"
  script:
    - import_assets artifacts/ubi/gitlab-exiftool.tar.gz
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - rust_version=$(cat artifacts/container_versions/gitlab-rust_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$ruby_version$rust_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
    - fi
    - pg_version=$(cat artifacts/container_versions/postgresql_tag.txt)
    - pg_image="${CI_REGISTRY_IMAGE}/postgresql:${pg_version}"
    - gei_dir_version=$(get_version gitlab-elasticsearch-indexer)
    - gei_version=($(echo -n "$GITLAB_ELASTICSEARCH_INDEXER_VERSION$go_version$gei_dir_version" | sha1sum))
    - gei_image="${CI_REGISTRY_IMAGE}/gitlab-elasticsearch-indexer:${gei_version}${IMAGE_TAG_EXT}"
    - gme_version=$(cat artifacts/container_versions/gitlab-metrics-exporter_tag.txt)
    - gme_image="${CI_REGISTRY_IMAGE}/gitlab-metrics-exporter:${gme_version}${IMAGE_TAG_EXT}"
    - gm_version=$(cat artifacts/container_versions/gitlab-graphicsmagick_tag.txt)
    - gm_image="${CI_REGISTRY_IMAGE}/gitlab-graphicsmagick:${gm_version}"
    - exiftool_version=$(cat artifacts/container_versions/gitlab-exiftool_tag.txt)
    - exiftool_image="${CI_REGISTRY_IMAGE}/gitlab-exiftool:${exiftool_version}"
    - docker pull $pg_image > /dev/null || true
    - export ASSETS_IMAGE="${ASSETS_IMAGE_REGISTRY_PREFIX}/${EE_PROJECT}/${ASSETS_IMAGE_PREFIX}-ee:${GITLAB_ASSETS_TAG}"
    - fetch_assets
    - build_if_needed --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "GITLAB_PROJECT=${EE_PROJECT}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "CACHE_BUSTER=$GITLAB_VERSION$(date -uI)"
                      --build-arg "TAG=$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "PG_IMAGE=$pg_image"
                      --build-arg "GEI_IMAGE=${gei_image}"
                      --build-arg "GME_IMAGE=${gme_image}"
                      --build-arg "ASSETS_IMAGE=${ASSETS_IMAGE}"
                      --build-arg "RUBY_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "RUST_TAG=${rust_version}${IMAGE_TAG_EXT}"
                      --build-arg "GM_IMAGE=${gm_image}"
                      --build-arg "EXIFTOOL_IMAGE=${exiftool_image}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   image_name=$(tail -n1 artifacts/images/$CI_JOB_NAME.txt)
    -   container_id=$(docker create "${CI_REGISTRY_IMAGE}/${image_name}")
    -   rm -rf ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   mkdir -p ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   docker cp $container_id:/srv/gitlab/vendor/bundle ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   docker rm -v $container_id
    - fi
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-rails-ee_tag.txt
  cache:
    key: "$CI_JOB_NAME-gitlab-rails-vendor-bundle-$RUBY_CACHE_KEY-$IMAGE_FLAVOR-$RAILS_CACHE_SUFFIX"
    paths:
      - gitlab-rails/vendor/bundle


.gitlab-rails-ce:
  extends:
    - .job-base
    - .except-ee
  stage: phase-five
  script:
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - rust_version=$(cat artifacts/container_versions/gitlab-rust_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$ruby_version$rust_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-ruby:$ruby_version"
    - pg_version=$(cat artifacts/container_versions/postgresql_tag.txt)
    - pg_image="${CI_REGISTRY_IMAGE}/postgresql:${pg_version}"
    - gme_version=$(cat artifacts/container_versions/gitlab-metrics-exporter_tag.txt)
    - gme_image="${CI_REGISTRY_IMAGE}/gitlab-metrics-exporter:${gme_version}${IMAGE_TAG_EXT}"
    - gm_version=$(cat artifacts/container_versions/gitlab-graphicsmagick_tag.txt)
    - gm_image="${CI_REGISTRY_IMAGE}/gitlab-graphicsmagick:${gm_version}"
    - exiftool_version=$(cat artifacts/container_versions/gitlab-exiftool_tag.txt)
    - exiftool_image="${CI_REGISTRY_IMAGE}/gitlab-exiftool:${exiftool_version}"
    - docker pull $pg_image > /dev/null || true
    - export ASSETS_IMAGE="${ASSETS_IMAGE_REGISTRY_PREFIX}/${CE_PROJECT}/${ASSETS_IMAGE_PREFIX}-ce:${GITLAB_ASSETS_TAG}"
    - fetch_assets
    - build_if_needed --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "GITLAB_PROJECT=${CE_PROJECT}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
                      --build-arg "CACHE_BUSTER=$GITLAB_VERSION$(date -uI)"
                      --build-arg "TAG=$ruby_version"
                      --build-arg "PG_IMAGE=$pg_image"
                      --build-arg "GME_IMAGE=${gme_image}"
                      --build-arg "ASSETS_IMAGE=${ASSETS_IMAGE}"
                      --build-arg "RUST_TAG=${rust_version}"
                      --build-arg "GM_IMAGE=${gm_image}"
                      --build-arg "EXIFTOOL_IMAGE=${exiftool_image}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   image_name=$(tail -n1 artifacts/images/$CI_JOB_NAME.txt)
    -   container_id=$(docker create "${CI_REGISTRY_IMAGE}/${image_name}")
    -   rm -rf ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   mkdir -p ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   docker cp $container_id:/srv/gitlab/vendor/bundle ${CI_PROJECT_DIR}/gitlab-rails/vendor
    -   docker rm -v $container_id
    - fi
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/gitlab-rails-ce_tag.txt
  cache:
    key: "$CI_JOB_NAME-gitlab-rails-vendor-bundle-$RUBY_CACHE_KEY-$IMAGE_FLAVOR-$RAILS_CACHE_SUFFIX"
    paths:
      - gitlab-rails/vendor/bundle

## phase six

.gitlab-geo-logcursor:
  extends:
    - .job-base
    - .only-ee
  stage: phase-six
  script:
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}

.gitlab-sidekiq-ee:
  extends:
    - .job-base
    - .only-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}

.gitlab-sidekiq-ce:
  extends:
    - .job-base
    - .except-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ce_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ce:$rails_container"
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ce"
                      --build-arg "PYTHON_TAG=${python_version}"
                      --build-arg "TAG=$rails_container"
                      --build-arg "GITLAB_LOGGER_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-logger:${GITLAB_LOGGER_VERSION}${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG

.gitlab-toolbox-ee:
  extends:
    - .job-base
    - .only-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - gitaly_container=$(cat artifacts/container_versions/gitaly_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "S3CMD_VERSION=$S3CMD_VERSION"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITALY_IMAGE=$CI_REGISTRY_IMAGE/gitaly:$gitaly_container${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT} ${CI_JOB_NAME/toolbox/task-runner}

.gitlab-toolbox-ce:
  extends:
    - .job-base
    - .except-ee
  stage: phase-six
  script:
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ce_tag.txt)
    - gitaly_container=$(cat artifacts/container_versions/gitaly_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ce:$rails_container"
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ce"
                      --build-arg "TAG=$rails_container"
                      --build-arg "PYTHON_TAG=${python_version}"
                      --build-arg "S3CMD_VERSION=$S3CMD_VERSION"
                      --build-arg "GITALY_IMAGE=$CI_REGISTRY_IMAGE/gitaly:$gitaly_container${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG
    - push_tags $GITLAB_REF_SLUG ${CI_JOB_NAME/toolbox/task-runner}

.gitlab-webservice-ee:
  extends:
    - .job-base
    - .only-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ee:$rails_container${IMAGE_TAG_EXT}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}

.gitlab-webservice-ce:
  extends:
    - .job-base
    - .except-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(cat artifacts/container_versions/gitlab-ruby_tag.txt)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ce_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-ce:$rails_container"
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-ce"
                      --build-arg "TAG=$rails_container"
                      --build-arg "PYTHON_TAG=${python_version}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG

.gitlab-workhorse-ce:
  extends:
    - .job-base
    - .except-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - rails_version=$(get_version gitlab-rails)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ce_tag.txt)
    - exiftool_version=$(cat artifacts/container_versions/gitlab-exiftool_tag.txt)
    - exiftool_image="${CI_REGISTRY_IMAGE}/gitlab-exiftool:${exiftool_version}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$go_version$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:$go_version"
    - build_if_needed --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=$GITLAB_VERSION"
                      --build-arg "RAILS_VERSION=${rails_container}"
                      --build-arg "GITLAB_EDITION=gitlab-rails-ce"
                      --build-arg "EXIFTOOL_IMAGE=${exiftool_image}"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG

.gitlab-workhorse-ee:
  extends:
    - .job-base
    - .only-ee
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-exiftool.tar.gz
    - import_assets artifacts/ubi/gitlab-gomplate.tar.gz
    - gitlab_base_version=$(cat artifacts/container_versions/gitlab-base_tag.txt)
    - export GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${gitlab_base_version}${IMAGE_TAG_EXT}"
    - rails_version=$(get_version gitlab-rails)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - exiftool_version=$(cat artifacts/container_versions/gitlab-exiftool_tag.txt)
    - exiftool_image="${CI_REGISTRY_IMAGE}/gitlab-exiftool:${exiftool_version}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$go_version$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go:$go_version${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "TAG=${gitlab_base_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=$GITLAB_VERSION"
                      --build-arg "RAILS_VERSION=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_EDITION=gitlab-rails-ee"
                      --build-arg "EXIFTOOL_IMAGE=${exiftool_image}"
                      --build-arg "GITLAB_BASE_IMAGE=${GITLAB_BASE_IMAGE}"
    - push_tags
    - push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
